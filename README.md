# Cells APP


Haciendo preubas para en cells disparar un evento en una pagina y escucharlo en otra  hemos encontrado un comportamiento poco claro y queremos saber si es un comportamiento esperado o  lo estamos haciendo mal 

cuando tenemos un out asi 
-
~~~ 
  "greeting-channel": {
    "bind": "greeting-changed"
  },
~~~

y un in asi 
-
~~~ 
  "greeting-channel": {
    "bind": "another-greeting"
  },
~~~

el valor pasado se pone como atributo en el html greeting-changed="bienvenido" por lo que polymer lo traslada a la propiedad anotherGreeting

pero cuando intentamos hacer lo mismo con un objeto {'saludo':'hola'}

el out es asi
-
~~~ 
  "tuyo-transanction-confirm-data": {
    "bind": "transfer-confirm-data-changed"
   },
~~~

y el in es asi 
-
~~~
  "tuyo-transanction-confirm-data": {
    "bind": "transfer-confirm-data"
  },
~~~

en el atributo pone transfer-confirm-data="[object Object]" y polymer lo traslada tal cual a la propiedad por lo que "parece" que funciona mal el bindeo


cuando utilizamos la porpiedad pero como JSON.stringify(this.response) polymer lo convierte al atributo transfer-confirm-data2="{"saludo":"hola"}" y si se parsea bien a la propiedad correspondiente.


si invocamos a una funcion el valor si llega correctamente como parametro a la funcion y no se pone el atributo en el html

el in es asi
-
~~~ 
  "tuyo-transanction-confirm-data3": {
    "bind": "transfer-confirm-data3-changed"
   }
~~~

el out es asi 
-
~~~ 
  "tuyo-transanction-confirm-data3": {
    "bind": "checkFunction"
  } 
~~~

y cuando utilizamos en el json el nombre de la propiedad en camelcase como esta en el js, no se pone el atributo en el html y se recibe bien en la propiedad.

el in es asi
-
~~~ 
  "tuyo-transanction-confirm-data4": {
    "bind": "transfer-confirm-data4-changed"
  },
~~~

el out es asi
-
~~~ 
  "tuyo-transanction-confirm-data4": {
    "bind": "transferConfirmData4"
  }
~~~

Entendemos que por performance lo ideal es usar cuando sea posible el caso 4 para evitar el poner el parametro como atributo y evitar conversiones de atributos con valores de objetos como cadenas de texto.

Pero necesitamos confirmación de que es el comportamiento esperado y la valoracion por parte del equipo de core sobre si podria ser razonable que:

"cuando un in en un objeto este de la forma kebab-case "tuyo-transanction-confirm-data4" pero existe una propiedad en el objeto destino en camelcase "transferConfirmData4" en vez de ponerlo como atributo se cambie y se comporte como si hubiesemos puesto 
-
~~~ 
  "tuyo-transanction-confirm-data4": {
    "bind": "transferConfirmData4"
  }
~~~

en vez de
-
~~~ 
  "tuyo-transanction-confirm-data4": {
    "bind": "transfer-confirm-data4-changed"
  },
~~~

